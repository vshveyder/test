<?php
session_start();

require_once '../components/db_connect.php';
require_once '../components/News.php';
$news = new News($db);
//проверка автризиции админа
if(isset($_SESSION['login'])){
    if($_SESSION['login'] != 'admin')
        header('Location: ../index.php');
    else{
        //delete
        if(isset($_GET['delete']) && $_GET['delete'] == 1 && isset($_GET['delete_id'])){
            $news->delete($_GET['delete_id']);
            header('Location: adminPanel.php?mes=delete');
        }
        //update
        if(isset($_GET['update'])){
            $news->update($_GET['id_update'],$_GET['head'],$_GET['text']);
            header('Location: adminPanel.php?mes=update');
        }
        //delete all
        if(isset($_GET['delete_all']) && $_GET['delete_all'] == 1){
            $news->delete_all();
            header('Location: adminPanel.php?mes=del_all');
        }
        //delete group
        if($_POST['delete_group'] == 1){
            $news->delete_group($_POST);
            header('Location: adminPanel.php?mes=del_a');
        }

    }
}
else    header('Location: ../index.php');
