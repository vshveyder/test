

function GetPost(cd,one) {

    const  xhr = new XMLHttpRequest();
    xhr.open('get','../news.php?id=' + one);
    xhr.addEventListener('load',() => {
        cd(JSON.parse(xhr.responseText));
        console.log(JSON.parse(xhr.responseText));
    });
    xhr.send();
}

let id=3;
function add() {
    GetPost(response =>{
        const card_id = document.getElementById('card_id');
        const card_i3 = document.createElement('div');
        card_i3.className = "row card_i3";

        response.forEach(post => {
            const card_i = document.createElement('div');
            card_i.className = "card card_i";

            const img = document.createElement('img');
            img.className = "card-img-top img_i";
            img.src = '../img/'+post.name_img+'jpg';

            const card_body = document.createElement('div');
            card_body.className = "card-body";
            const card_title = document.createElement('h5');
            card_title.className = "card-title";
            card_title.innerHTML = post.headline;
            const card_text = document.createElement('p');
            card_text.className = "card-text";
            card_text.innerHTML = post.full_text;

            const link = document.createElement('a');
            link.className = "btn btn-primary a_marg";
            link.innerHTML = 'Read';
            link.href = post.head_link;

            const p_data = document.createElement('p');
            p_data.className = "card-text text-muted";
            p_data.innerHTML = post.data;

            const link_del = document.createElement('a');
            link_del.className = "btn btn-danger";
            link_del.style = "margin: 0 5px;";
            link_del.innerHTML = 'Delete';
            link_del.href = 'action.php?delete=1&delete_id='+post.id;
            link_del.toggleAttribute('onclick');
            link_del.setAttribute('onclick', 'f()');


            const link_update = document.createElement('a');
            link_update.className = "btn btn-success a_marg";
            link_update.innerHTML = 'Update';
            link_update.href = 'Update.php?update=1&id_update='+post.id+'&head='+post.headline+'&text='+post.full_text;


            let checkbox = document.createElement("input");
            checkbox.type= "checkbox";
            checkbox.name = post.id;
            checkbox.value = post.id;
            checkbox.className = "option-input";
            checkbox.toggleAttribute('onchange');
            checkbox.setAttribute('onchange', 'check()');

            let a_group = document.createElement('div')
            a_group.className = "a_group";

            card_body.appendChild(p_data);
            card_body.appendChild(card_title);
            card_body.appendChild(card_text);

            a_group.appendChild(link);
            a_group.appendChild(link_del);
            a_group.appendChild(link_update);
            a_group.appendChild(checkbox);
            card_body.appendChild(a_group);

            card_i.appendChild(img);
            card_i.appendChild(card_body);
            card_i3.appendChild(card_i);
        });

        card_id.appendChild(card_i3);
        console.log(card_i3);
    },id);
    id += 3;
}

function check() {
    let checked  = document.querySelectorAll('.option-input');
    let  bt2 = document.getElementById('id_on3');
    let  bt1 = document.getElementById('id_on2');
    let  bt = document.getElementById('id_on1');
    let but = document.getElementById('id_on');
    let res = 0;
    checked.forEach(post => {
        if(post.checked){
            res+=1;
        }
    });
    if(res > 0){
        but.style = "display: inline;";
        bt.style = "display: inline;";
        bt1.style = "display: inline;";
        bt2.style = "display: inline;";
    }else {
        but.style = "display: none;";
        bt.style = "display: none;";
        bt1.style = "display: none;";
        bt2.style = "display: none;";
    }
    // let button = document.querySelector('#id_on');
    // console.log(button);
    // button.style = 'display:block;';
}


//Всплывает окно, запрашиает переход по ссылке, если нажать отмена то не переходит.
function f(event) {
    if (confirm('Вы уверены, что хотите удалить эту новость?'))
        return true;
    else {
        (event || window.event).preventDefault()
        return false;
    }
}
function f1(event) {
    if (confirm('Експортировать?')){
        let form = document.getElementById('for_m');
    form.action = "../export/export.php";
    return true;
}
    else {
        (event || window.event).preventDefault()
        return false;
    }
}
function f2(event) {
    if (confirm('Експортировать все?')){
        let form = document.getElementById('for_m');
        form.action = "../export/export.php";
        return true;
    }
    else {
        (event || window.event).preventDefault()
        return false;
    }
}