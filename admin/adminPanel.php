<?php
session_start();
if (!isset($_SESSION['login']) || $_SESSION['login'] != 'admin') {
    header("Location: ../index.php");
}
require_once '../components/db_connect.php';
require_once '../components/News.php';
$news = ((new News($db))->news);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/png" href="icons8.png">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="../components/style.css">
    <title>Admin Panel</title>
</head>
<body>
<?php include '../components/header.php';?>
<div class="container cont">
    <?php if(isset($_GET['export'])): ?>
        <div class="alert alert-primary" role="alert">
            <p>
              файл создан. Путь к файлу: test/export/<?=$_GET['export']?>
            </p>
        </div>
    <?php endif; ?>
    <form action="action.php" method="post" id="for_m">
        <div class="row" id="card_id">

            <div class="row card_i3" id="card_i3">
                <?php
                if(!empty($news)):
                for ($i=0; $i<=2; $i++):?>
                    <div class="card card_i"  id="card_i">
                        <img src="../img/<?=$news[$i]['name_img']?>jpg" class="card-img-top img_i" alt="...">
                        <div class="card-body">

                            <p class="card-text text-muted"><?=$news[$i]['data']?></p>
                            <h5 class="card-title"><?=$news[$i]['headline']?></h5>
                            <p class="card-text text_p"><?=$news[$i]['full_text']?></p>
                            <div class="a_group">
                                <a class="btn btn-primary a_marg " href="<?=$news[$i]['head_link']?>">Read</a>
                                <a href="action.php?delete=1&delete_id=<?= $news[$i]['id'] ?>" class="btn btn-danger" id="delete" onclick="f()">Delete</a>
                                <a href="Update.php?update=1&id_update=<?= $news[$i]['id'] ?>&head=<?= $news[$i]['headline'] ?>&text=<?= $news[$i]['full_text'] ?>"
                                   class="btn btn-success">Update</a>
                                <label>
                                    <input type="checkbox" class="option-input" name="<?=$news[$i]['id']?>" value="<?=$news[$i]['id']?>" onchange="check()">
                                </label>
                            </div>
                         </div>
                    </div>
                <?php endfor;
                endif;
                ?>
            </div>
        </div>
        <div class="btnnn">
            <button name="delete_group" value="<?=true?>" id = "id_on" class="exp" onclick="f()" style="display: none">Delete</button>
            <button name="export_csv" value="<?=true?>" id = "id_on1" class="exp" onclick="f1()" style="display: none">exp:csv</button>
            <button name="export_xls" value="<?=true?>" id = "id_on2" class="exp" onclick="f1()" style="display: none">exp:xlsx</button>
            <button name="export_pdf" value="<?=true?>" id = "id_on3" class="exp" onclick="f1()" style="display: none">exp:pdf</button>
        </div>
    </form>
    <input type="button" class="btn btn-success btn_marg" value="add" onclick="add()">

    <div class="export">
        <a class="btn btn-danger  " href="action.php?delete_all=1" onclick="f()">Delete all elements</a>
        <a class="btn btn-secondary  " href="../export/export.php?exp_all_csv=1" onclick="f2()">export all csv</a>
        <a class="btn btn-secondary  " href="../export/export.php?exp_all_xls=1" onclick="f2()">export all xlsx</a>
        <a class="btn btn-secondary  " href="../export/export.php?exp_all_pdf=1" onclick="f2()">export all pdf</a>
    </div>
    <script src="Adminjs.js"></script>
</div>
</body>
</html>
