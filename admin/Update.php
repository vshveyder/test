<?php
session_start();
if(!isset($_SESSION['login']) || !isset($_GET['update']))
    header('Location: index.php');
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/png" href="../icons.png">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="../components/style.css">
    <title>news</title>
</head>
<body>
    <?php include '../components/header.php' ?>
    <div class="container cont">
        <form action="action.php" method="get">
            <div class="form-group">
                <label for="exampleFormControlInput1">headline</label>
                <textarea name="head" type="text" class="form-control form-mob" id="exampleFormControlInput1" ><?=$_GET['head']?></textarea>
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">full_text</label>
                <textarea  name="text" class="form-control form-mob" id="exampleFormControlTextarea1" rows="3"><?=$_GET['text']?></textarea>
            </div>
            <input name="id_update" value="<?=$_GET['id_update']?>" style="display: none">
            <input name="update" value="1" style="display: none">
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</body>
</html>
