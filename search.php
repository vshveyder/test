<?php
require_once 'components/db_connect.php';
require_once 'components/News.php';
if(isset($_GET['search'])){
    $news = new News($db);
    $news_id = $news->search($_GET['search']);
}   else header('Location: index.php');

if(empty($news_id)){
    $mess = 'Ничего не найдено!';
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/png" href="icons9.png">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="components/style.css">
    <title>news</title>
</head>
<body>
<?php include 'components/header.php' ?>
<div class="container cont">
    <div class="row">
    <?php
    if(!empty($news_id)):
    foreach ($news_id as $value):?>
        <div class="card mb-3">
            <img src="img\<?=$value['name_img']?>jpg" class="card-img-top size" alt="...">
            <div class="card-body">
                <h5 class="card-title"><a href="<?=$value['head_link']?>" class="badge"><?=$value['headline']?></a></h5>
                <p class="card-text"><?=$value['full_text']?></p>
                <p class="card-text"><small class="text-muted"><?=$value['data']?></small></p>
            </div>
        </div>
    <?php endforeach;
    else:
    ?>
    <div class="alert alert-primary" role="alert">
        <p><?=$mess?>
        </p>
    </div>
    <?php endif; ?>
    </div>
</div>
</body>
</html>

