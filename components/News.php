<?php

class News
{
    public $db;
    public $news;


    public function __construct(PDO $db, $arr = NULL)
    {
        if ($arr == NULL) {
            $this->db = $db;
            $query = $db->query('select * from news');
            $this->news = $query->fetchAll();
        } else {
            // $arr - массив id которые соответсвуют id  в бд
            //ключ массива должен быть цыфрой
            $this->news=[];
            $query = 'SELECT * FROM news WHERE id=:id';
            foreach ($arr as $key => $value)
                if (is_int($key)){
                    $query_p = $db->prepare($query);
                    $query_p->bindValue(':id', $value);
                    $query_p->execute();
                    $this->news[] = $query_p->fetch();
            }
        }
    }



    public function update($id, $head, $text)
    {
        $query = 'UPDATE news 
                    SET headline = :head, full_text = :text
                    WHERE id = :id;
';
        $query_p = $this->db->prepare($query);
        $query_p->bindValue(':id', $id);
        $query_p->bindValue(':head', $head);
        $query_p->bindValue(':text', $text);
        $query_p->execute();
    }

    public function delete_all()
    {
        $query = 'TRUNCATE news';
        $this->db->exec($query);
        foreach ($this->news as $value) {
            file_exists('../img/' . $value['name_img'] . 'jpg') ?
                unlink('../img/' . $value['name_img'] . 'jpg') : false;
        }
    }

    public function delete($id)
    {
        $query = 'delete from news where id = :id';
        $query_p = $this->db->prepare($query);
        $query_p->bindValue(':id', $id);
        $query_p->execute();
    }

    public function search($str)
    {
        $arr = [];
        foreach ($this->news as $value) {
            $s = stristr($value['headline'], $str);
            if ($s !== false) {
                $arr[] = $value;
            }
        }
        return $arr;

    }

    public function delete_group($arr)
    {
        $query = "DELETE FROM news WHERE id = :id";
        foreach ($arr as $key => $value) {
            if (is_int($key)) {
                $query_p = $this->db->prepare($query);
                $query_p->bindValue(':id', $value);
                $query_p->execute();

                file_exists('../img/' . $this->news[$value]['name_img'] . 'jpg') ?
                    unlink('../img/' . $this->news[$value]['name_img'] . 'jpg') : false;
            }
        }
    }
}