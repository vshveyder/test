<?php

try {
    $db = new PDO(
        'mysql:host=localhost;dbname=test_task',
        'root',
        'root'
    );
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (Exception $e) {
    echo "ERROR: Could not connect to the database";
    die();
}
