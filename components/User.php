<?php


class User
{
    public $db;

    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    public function register($name,$login,$pass){
        $chek = 'Select login from user where login  = :login';
        $query_r = $this->db->prepare($chek);
        $query_r->bindValue(':login', $login);
        $query_r->execute();
        $chek1 = $query_r->fetch();
        if(!$chek1) {
            $query = '
        INSERT INTO user SET
        name = :name,
        login = :login,
        password = :password 
        ';
            try {
                $query_r = $this->db->prepare($query);
                $query_r->bindValue(':login', $login);
                $query_r->bindValue(':name', $name);
                $query_r->bindValue(':password', $pass);
                $query_r->execute();
            } catch (Exception $e) {
                die();
                echo 'ERROR <a href="http://test.loc/index.php">go home</a>';
            }
        return true;
        }
        else return false;
    }

    public function in($login, $pass){
        $query = '
        SELECT * FROM user where login = :login AND password = :pass
        ';
        $query_r = $this->db->prepare($query);
        $query_r->bindValue(':login', $login);
        $query_r->bindValue(':pass', $pass);
        $query_r->execute();
        $arr = $query_r->fetch();
        return !empty($arr) ? $arr['name'] : false;
    }
}