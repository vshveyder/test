<?php
function active($str)
{
    preg_match('#'.$str.'#su',$_SERVER['REQUEST_URI'],$res);

    return $res ? 'active':'';

}
?>
<nav class="bg-dark bg-nav">
    <a class="nav_a" href="/index.php">NEWS</a>
    <label class="lab_name"> <?=isset($_SESSION['name'])?'You:'.$_SESSION['name']:''?></label>
    <div class="div-nav" id="navbarTogglerDemo02">
        <input type="checkbox" name="toggle" class="menu" id="menu">
        <label for="menu" class="menu">Menu</label>
        <ul class="Ul-nav">
            <?php
            echo isset($_SESSION['login']) ?
            '<li class="'.active("adminPanel").'">
                <a class="nav-link" href="/admin/adminPanel.php">ADMIN PANEL<span class="sr-only">(current)</span></a>
            </li>': '';

            echo isset($_SESSION['name']) ?
                '<li class="name">
                <a class="nav-link" href="#">Your name: ('.$_SESSION['name'].')</a>
            </li>'
                :'<li class="'.active('registr').'">
                <a class="nav-link" href="/reg/registr.php">Register</a>
            </li>';
            ?>
            <li class="<?=active("input")?>">
                <?php
                echo isset($_SESSION['login']) ?
                    '<a class="nav-link" href="/out.php?out=1122334455">log out '.$_SESSION['login'].'</a>':
                    '<a class="nav-link " href="/input.php">Sing in</a>';
                ?>
            </li>
            <li class="nav-item ">
                <a class="nav-link text-danger li_li" href="/parser.php">PARSER</a>
            </li>
        </ul>
        <div class="search">
            <form class="form-inline my-2 my-lg-0" action="/search.php" method="get">
                <input class="form-control mr-sm-2" type="search" placeholder="Search by title" name="search" value="<?=isset($_GET['search'])?$_GET['search']:''?>" required>
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </div>
</nav>
