<?php
require_once '../components/User.php';
require_once '../components/db_connect.php';
if(isset($_POST['register'])){
    if($_POST['register'] == 'to register'){
        $user = new User($db);
       $res = $user->register($_POST['name'],$_POST['login'],$_POST['password'])? 'your register!':'error: a user with this login is already register';
       echo $res;
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="../components/style.css">
    <title>news</title>
</head>
<body>
<?php include '../components/header.php' ?>
    <div class="container">
        <form method="post" action="registr.php">
            <div class="form-group">
                <label for="formGroupExampleInput">Name</label>
                <input name="name" type="text" class="form-control" id="formGroupExampleInput" placeholder="your name" required>
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">Login</label>
                <input name="login" type="text" class="form-control" id="formGroupExampleInput2" placeholder="login" required>
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput3">Password</label>
                <input name="password" type="text" class="form-control" id="formGroupExampleInput3" placeholder="*****" required>
            </div>
            <button name="register" type="submit" class="btn btn-primary btn_reg" value="to register">to register</button>
        </form>
    </div>
</body>
</html>
