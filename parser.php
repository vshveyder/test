<?php
require_once('components/db_connect.php');
$query = '
    INSERT INTO news SET
    headline = :headline,
    head_link = :head_link,
    data = :data,
    full_text = :full_text,
    name_img = :name_img
';
$page = file_get_contents("http://www.dailynebraskan.com/news/");

preg_match_all('#<article[^>]+?class\s*?=\s*?(["\'])clearfix card summary has-image image-left inherit  tnt-section-news\1[^>]*?>(.*?)</article>#su', $page, $str);

if($db->query('SELECT * FROM news')->fetch()) {
    $str2 = array_reverse($str[2]);
} else $str2 = $str[2];
foreach ($str2 as $value) {
    //img
    preg_match_all('#<img.+?srcset\s*?=\s*?(["\'])(.*?)\1[^>]*?>#su', $value, $res);
    preg_match_all('#(.*?)\?#su', $res[2][0], $link); // link photo -> $link[0][0]
    preg_match_all('#([^/]+)\.#su', $link[0][0], $name_img); //img Name -> $name_img[0][2]
    //headline
    preg_match_all('#<h3[^>]+?class\s*?=\s*?(["\'])tnt-headline \1[^>]*?>.*?<a[^>]+>\s+(.*?)</a>.*?</h3>#su', $value, $headline); // headline -> $headline[2][0]
    // link head
    preg_match_all('#<h3[^>]+?class\s*?=\s*?(["\'])tnt-headline \1[^>]*?>.*?<a[^>]+?href\s*?=\s*?(["\'])(.*?)\1>.*?</a>.*?</h3>#su', $value, $head_link); // head_link -> $head_link[3][0]
    //data
    preg_match_all('#<time[^>]+?>(.*?)</time>#su', $value, $data); // data -> $data[1][0]
    //full_text
    preg_match_all('#<p[^>]+?>(.*?)</p>#', $value, $full_text); //full_text -> $full_text[1][0]



    if(!check($db,$headline[2][0])) {
        $query_p = $db->prepare($query);
        $query_p->bindValue(':headline', $headline[2][0]);
        $query_p->bindValue(':head_link', 'http://www.dailynebraskan.com'.$head_link[3][0]);
        $query_p->bindValue(':data', $data[1][0]);
        $query_p->bindValue(':full_text', $full_text[1][0]);
        $query_p->bindValue(':name_img', $name_img[0][2]);
        $query_p->execute();
        copy($link[0][0],'img/'.$name_img[0][2].'jpg');
    }
}

function check($db,$head)
{
    $f = $db->prepare('select headline from news where headline like :headline');
    $f->bindValue(':headline', $head);
    $f->execute();
    return $f->fetch();
}
header('Location: index.php');