<?php
session_start();
require_once 'components/User.php';
require_once 'components/db_connect.php';
if(isset($_POST['login']) && $_POST['login'] == 'admin'){
    if(isset($_POST['password']) && $_POST['password'] == 'admin'){
        $_SESSION['login'] = 'admin';
        $_SESSION['name'] = 'Admin';
        header("Location: admin/adminPanel.php");
    }
} elseif (isset($_POST['login']) && $_POST['login'] != 'admin'){
    $user = new User($db);
    $_SESSION['name'] =  $user->in($_POST['login'],$_POST['password']);
    $_SESSION['login'] = 'user';
}
if(isset($_SESSION['login'])){
    header("Location: admin/adminPanel.php");
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="components/style.css">
    <title>news</title>
</head>
<body>
<?php
include 'components/header.php';
?>
<div class="container">

    <div class="input_form">
        <form action="#" method="post">
            <div class="form-group">
                <label for="exampleInputEmail1">Login</label>
                <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="login" required>
                <small id="emailHelp" class="form-text text-muted">We'll never share your login with anyone else.</small>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" name="password" required>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
    <br>
    <div class="alert alert-primary" role="alert">
        <p>
            Логин от админки : admin<br>
            Пароль от админки: admin<br>


        </p>
    </div>
</div>
</body>
</html>