<?php
session_start();
require_once('components/db_connect.php');
require_once('components/News.php');
//require_once('parser.php');
$news = ((new News($db))->news);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/png" href="icons8.png">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="components/style.css">
    <title>news</title>
</head>
<body>
<?php include 'components/header.php';?>
    <div class="container cont">
        <div class="row" id="card_id">
            <div class="row card_i3" id="card_i3">
                <?php
                if(!empty($news)):
                for ($i=0; $i<=2; $i++):?>
                <div class="card card_i" id="card_i">
                    <img src="img/<?=$news[$i]['name_img']?>jpg" class="card-img-top img_i" alt="...">
                    <div class="card-body">

                        <p class="card-text text-muted"><?=$news[$i]['data']?></p>
                        <h5 class="card-title"><?=$news[$i]['headline']?></h5>
                        <p class="card-text"><?=$news[$i]['full_text']?></p>
                        <a class="btn btn-primary a_marg" href="<?=$news[$i]['head_link']?>">Read</a>

                    </div>
                </div>
                <?php
                endfor;
                endif;
                ?>
            </div>
        </div>
        <input type="button" class="btn btn-success btn_marg" value="add" onclick="add()">
        <script src="components/add_el.js"></script>
    </div>
</body>
</html>
