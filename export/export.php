<?php
require_once('../components/db_connect.php');
require_once('../components/News.php');

require('../vendor/fpdf/fpdf.php');
require_once('../vendor/Excel/PHPExcel.php');

if(isset($_POST['export_csv']) || isset($_POST['export_xls']) || isset($_POST['export_pdf']))
    $news = ((new News($db,$_POST))->news);
else $news = ((new News($db))->news);
//var_dump($news);

if(isset($_POST['export_csv']) && !empty($news)){
    if($_POST['export_csv'] == 1){
        $filename = 'export_0'.rand(0,9).rand(9,99).rand(0,9).rand(9,99).'.csv';
        $fp = fopen($filename,'w+');

        foreach ($news as $val) {
            $arr = [];
            foreach ($val as $key => $value)
                if (!is_int($key)) {
                 $arr += [$key => $value];
                }
//            var_dump($arr);

                fputcsv($fp, $arr);
        }
        fclose($fp);
        ob_end_clean();
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream, charset=UTF-8');
        header('Content-Disposition: attachment; filename="'.basename($filename).'"');
        header('Expires: 0');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: no-cache');
        header('Content-Length: ' . filesize($filename));
        readfile($filename);
        exit;
    }
} elseif (isset($_POST['export_pdf']) && !empty($news)){
    $filename = 'export_0' . rand(0, 9) . rand(9, 99) . rand(0, 9) . rand(9, 99) . '.pdf';
    $pdf = new FPDF();
    foreach ($news as $value) {
            $pdf->AddPage();
            $pdf->Image('../img/' . $value['name_img'] . 'jpg', 5, 2, 200);
            $pdf->SetFont('Arial', 'B', 22);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Ln(138);
            $pdf->MultiCell(185, 10, $value['headline']);
            $pdf->Ln(3);
            $pdf->SetFont('Arial', '', 18);
            $pdf->MultiCell(185, 10, $value['full_text']);
            $pdf->SetTextColor(128, 128, 128);
            $pdf->SetFont('Arial', '', 15);
            $pdf->Cell(20, 20, $value['data']);
    }
    ob_end_clean();
    $pdf->Output('I',$filename);
    header('Content-Description: File Transfer');
    header('Content-Type: application/pdf, charset=UTF-8');
    header('Content-Disposition: attachment; filename='.basename($filename));
    header('Expires: 0');
    header('Content-Transfer-Encoding: binary');
    header('Cache-Control: must-revalidate');
    header('Pragma: no-cache');
    header('Content-Length: '.filesize($filename));
    readfile($filename);
    exit;

} elseif (isset($_POST['export_xls']) && !empty($news)){
    if($_POST['export_xls'] == 1) {
        $filename = 'export_0' . rand(0, 9) . rand(9, 99) . rand(0, 9) . rand(9, 99) . '.xlsx';

        //Создаем экземпляр класса электронной таблицы
        $xls = new PHPExcel();
        //Получаем текущий активный лист
        //Получаем текущий активный лист
        try {
            $xls->setActiveSheetIndex(0);
        } catch (Exception $e) {
            echo 'pizdec';
        }
        $sheet = $xls->getActiveSheet() ;
        $i = 1;
        foreach ($news as  $value) {
                $sheet->setCellValue('A' . $i, $value[0]);
                $sheet->setCellValue('B' . $i, $value[1]);
                $sheet->setCellValue('C' . $i, $value[2]);
                $sheet->setCellValue('D' . $i, $value[3]);
                $sheet->setCellValue('E' . $i, $value[4]);
                $sheet->setCellValue('F' . $i, $value[5]);
                $i++;
        }
        // Выводим HTTP-заголовки
        header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
        header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
        header ( "Cache-Control: no-cache, must-revalidate" );
        header ( "Pragma: no-cache" );
        header ( "Content-type: application/vnd.ms-excel" );
        header ( 'Content-Disposition: attachment; filename='.basename($filename));
        // Выводим содержимое файла
        $objWriter = new PHPExcel_Writer_Excel5($xls);
        $objWriter->save('php://output');
    }
} elseif (isset($_GET['exp_all_csv']) && !empty($news)) {
    $filename = 'export_all_0' . rand(0, 9) . rand(9, 99) . rand(0, 9) . rand(9, 99) . '.csv';
    $fp = fopen($filename, 'w+');
    foreach ($news as $value)
        fputcsv($fp, $value);
    fclose($fp);
    ob_end_clean();
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream, charset=UTF-8');
    header('Content-Disposition: attachment; filename="'.basename($filename).'"');
    header('Expires: 0');
    header('Content-Transfer-Encoding: binary');
    header('Cache-Control: must-revalidate');
    header('Pragma: no-cache');
    header('Content-Length: ' . filesize($filename));
    readfile($filename);
    exit;
}  elseif (isset($_GET['exp_all_xls']) && !empty($news)){
    $filename = 'export_all_0' . rand(0, 9) . rand(9, 99) . rand(0, 9) . rand(9, 99) . '.xlsx';
    //Создаем экземпляр класса электронной таблицы
    $xls = new PHPExcel();
    //Получаем текущий активный лист
    try {
        $xls->setActiveSheetIndex(0);
    } catch (Exception $e) {
        echo 'pizdec';
    }
    $sheet = $xls->getActiveSheet() ;
    $i = 1;
    foreach ($news as $value) {
            $sheet->setCellValue('A' . $i, $value[0]);
            $sheet->setCellValue('B' . $i, $value[1]);
            $sheet->setCellValue('C' . $i, $value[2]);
            $sheet->setCellValue('D' . $i, $value[3]);
            $sheet->setCellValue('E' . $i, $value[4]);
            $sheet->setCellValue('F' . $i, $value[5]);
            $i++;
    }
    // Выводим HTTP-заголовки
    header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
    header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
    header ( "Cache-Control: no-cache, must-revalidate" );
    header ( "Pragma: no-cache" );
    header ( "Content-type: application/vnd.ms-excel" );
    header ( 'Content-Disposition: attachment; filename='.basename($filename));
    // Выводим содержимое файла
    $objWriter = new PHPExcel_Writer_Excel5($xls);
    $objWriter->save('php://output');

} elseif (isset($_GET['exp_all_pdf']) && !empty($news)){
    $filename = 'export_all_0' . rand(0, 9) . rand(9, 99) . rand(0, 9) . rand(9, 99) . '.pdf';
    $pdf = new FPDF();
    foreach ($news as $key => $value) {
        if (is_int($key)) {
            $pdf->AddPage();
            $pdf->Image('../img/' . $value['name_img'] . 'jpg', 5, 2, 200);
            $pdf->SetFont('Arial', 'B', 22);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Ln(138);
            $pdf->MultiCell(185, 10, $value['headline']);
            $pdf->Ln(3);
            $pdf->SetFont('Arial', '', 18);
            $pdf->MultiCell(185, 10, $value['full_text']);
            $pdf->SetTextColor(128, 128, 128);
            $pdf->SetFont('Arial', '', 15);
            $pdf->Cell(20, 20, $value['data']);
        }
    }
    $pdf->Output('I',$filename);
    ob_end_clean();
    header('Content-Description: File Transfer');
    header('Content-Type: application/pdf, charset=UTF-8');
    header('Content-Disposition: attachment; filename='.basename($filename));
    header('Expires: 0');
    header('Content-Transfer-Encoding: binary');
    header('Cache-Control: must-revalidate');
    header('Pragma: no-cache');
    header('Content-Length: ' . filesize($filename));
    readfile($filename);
    exit;
}


